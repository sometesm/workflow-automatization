import flask
import json
from flask import render_template
from flask import request

from engine.square import Square
from engine.rhombus import Rhombus


shapes = {}


with open("shapes.json") as f:
    shapes_json = json.load(f)


app = flask.Flask(__name__)


@app.route(rule="/save_position_of_shape/<id_of_shape>", methods=["POST"])
def save_position_of_shape(id_of_shape):
    shapes_json[id_of_shape]["position"]["x"] = request.form["x"]
    shapes_json[id_of_shape]["position"]["y"] = request.form["y"]

    with open("shapes.json", "w") as f:
        json.dump(shapes_json, f, indent=4)

    return '{ "success": true, "message": "Position of ' + id_of_shape + ' saved" }'


@app.route(rule="/get_form/<id_of_shape>", methods=["POST"])
def get_form(id_of_shape):
    shape = shapes_json[id_of_shape]
    return shapes[id_of_shape].get_form(shape, shapes_json)


@app.route(rule="/save_info/<id_of_shape>", methods=["POST"])
def save_info(id_of_shape):
    return shapes[id_of_shape].save_info(request, shapes_json)


@app.route(rule="/get_square", methods=["POST"])
def get_square():
    id = request.form["title"]
    description = request.form["description"]
    executor = request.form["executor"]

    shapes[id] = Square(id)

    return render_template(
        "shapes/square.html",
        title=id,
        description=description,
        executor=executor
    )


@app.route(rule="/get_rhombus", methods=["POST"])
def get_rhombus():
    id = request.form["title"]
    description = request.form["description"]
    executor = request.form["executor"]

    shapes[id] = Square(id)

    return render_template(
        "shapes/rhombus.html",
        title=id,
        description=description,
        executor=executor
    )





@app.route(rule="/", methods=["POST", "GET"])
def main():
    return render_template(
        'bs.html',
        shapes=shapes_json
    )

app.run(host="127.0.0.1", port=8090, debug=True)
