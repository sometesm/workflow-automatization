function Position(top, left, height, width) {
    this.top = top;
    this.left = left;
    this.height = height;
    this.width = width;
}


function Line(id, from_shape_id, to_shape_id, container) {
    var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    line.setAttribute('id', id);
    line.setAttribute("stroke-width", "2");
    line.setAttribute('stroke','black');

    line.setAttribute('marker-end','url(#MarkerArrow)');
    line.setAttribute('fill','none');

    var from_shape_type = shapes.get_shape_type(from_shape_id);
    var to_shape_type = shapes.get_shape_type(to_shape_id);

    /*
    Start count from left to top
    */

    var LEFT_UP = 2;      var MIDDLE_UP = 3;      var RIGHT_UP = 4;
    var LEFT_MIDDLE = 1;  var CENTER_MIDDLE = 0;  var RIGHT_MIDDLE = 5;
    var LEFT_DOWN = 8;    var MIDDLE_DOWN = 7;    var RIGHT_DOWN = 6;

    var from_diff_x = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    var from_diff_y = [0, 0, 0, 0, 0, 0, 0, 0, 0];

    var to_diff_x = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    var to_diff_y = [0, 0, 0, 0, 0, 0, 0, 0, 0];

    if(from_shape_type == "square") {
        from_diff_x = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        from_diff_y = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
    if(to_shape_type == "square") {
        to_diff_x = [0];
        to_diff_y = [0];
    }
    if(from_shape_type == "rhombus") {
        from_diff_x = [0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0];
        //from_diff_x = [0, -9,  0,  0,  0, 9, 2, 2, 0];
        //from_diff_y = [0,  0, -4, -4, -4, 0, 6, 6, 5];
        from_diff_y = [0,  0, 0,0,0,0,0,0,0,0,0,0,0];
    }
    if(to_shape_type == "rhombus") {
        to_diff_x = [0];
        to_diff_y = [0];
    }


    function from_square_above(from_square, to_square) {
        var ans = false;

        if(from_square.top + from_square.height < to_square.top) {
            ans = true;
        }
        return ans;
    }
    function from_square_below(from_square, to_square) {
        var ans = false;

        if(from_square.top > to_square.top + to_square.height) {
            ans = true;
        }
        return ans;
    }
    function from_square_from_left(from_square, to_square) {
        var ans = false;

        if(from_square.left + from_square.width < to_square.left) {
            ans = true;
        }
        return ans;
    }
    function from_square_from_right(from_square, to_square) {
        var ans = false;

        if(from_square.left > to_square.left + to_square.width) {
            ans = true;
        }
        return ans;
    }


    this.draw = function() {
        var $1 = $("#" + from_shape_id);
        var $1p = $1.position();
        var from_square = new Position($1p.top, $1p.left, $1.height(), $1.width());

        var $2 = $("#" + to_shape_id);
        var $2p = $2.position();
        var to_square = new Position($2p.top, $2p.left, $2.height(), $2.width());

        //
        // from_square  to_square
        //

        if(     from_square_from_left(from_square, to_square)
            && !from_square_above(from_square, to_square)
            && !from_square_below(from_square, to_square)
        ) {
            line.setAttribute('x1', from_square.left + from_square.width + from_diff_x[LEFT_MIDDLE]);
            line.setAttribute('y1', from_square.top + from_square.height/2 + from_diff_y[LEFT_MIDDLE]);
            line.setAttribute('x2', to_square.left + to_diff_x[CENTER_MIDDLE]);
            line.setAttribute('y2', to_square.top + to_square.height/2 + to_diff_y[CENTER_MIDDLE]);
        }

        // from_square
        //              to_square
        //

        else if(from_square_above(from_square, to_square)
            &&  from_square_from_left(from_square, to_square)
        ) {
            line.setAttribute('x1', from_square.left + from_square.width/2 + from_diff_x[LEFT_UP]);
            line.setAttribute('y1', from_square.top + from_square.height + from_diff_y[LEFT_UP]);
            line.setAttribute('x2', to_square.left + to_diff_x[CENTER_MIDDLE]);
            line.setAttribute('y2', to_square.top + to_square.height/2 + to_diff_y[CENTER_MIDDLE]);
        }

        //             from_square
        //              to_square
        //

        else if(from_square_above(from_square, to_square)
            &&  !from_square_from_left(from_square, to_square)
            &&  !from_square_from_right(from_square, to_square)
        ) {
            line.setAttribute('x1', from_square.left + from_square.width/2 + from_diff_x[MIDDLE_UP]);
            line.setAttribute('y1', from_square.top + from_square.height + from_diff_y[MIDDLE_UP]);
            line.setAttribute('x2', to_square.left + to_square.width/2 + to_diff_x[CENTER_MIDDLE]);
            line.setAttribute('y2', to_square.top + to_diff_y[CENTER_MIDDLE]);
        }

        //                          from_square
        //              to_square
        //

        else if(from_square_above(from_square, to_square)
            &&  from_square_from_right(from_square, to_square)
        ) {
            line.setAttribute('x1', from_square.left + from_square.width/2 + from_diff_x[RIGHT_UP]);
            line.setAttribute('y1', from_square.top + from_square.height + from_diff_y[RIGHT_UP]);
            line.setAttribute('x2', to_square.left + to_square.width + to_diff_x[CENTER_MIDDLE]);
            line.setAttribute('y2', to_square.top + to_square.height/2 + to_diff_y[CENTER_MIDDLE]);
        }

        //
        //              to_square  from_square
        //


        else if(from_square_from_right(from_square, to_square)
            &&  !from_square_above(from_square, to_square)
            &&  !from_square_below(from_square, to_square)
        ) {
            line.setAttribute('x1', from_square.left + from_diff_x[RIGHT_MIDDLE]);
            line.setAttribute('y1', from_square.top + from_square.height/2 + from_diff_y[RIGHT_MIDDLE]);
            line.setAttribute('x2', to_square.left + to_square.width + to_diff_x[CENTER_MIDDLE]);
            line.setAttribute('y2', to_square.top + to_square.height/2 + to_diff_y[CENTER_MIDDLE]);
        }

        //
        //              to_square
        //                         from_square

        else if(from_square_from_right(from_square, to_square)
            &&  from_square_below(from_square, to_square)
        ) {
            line.setAttribute('x1', from_square.left + from_square.width/2 + from_diff_x[RIGHT_DOWN]);
            line.setAttribute('y1', from_square.top + from_diff_y[RIGHT_DOWN]);
            line.setAttribute('x2', to_square.left + to_square.width + to_diff_x[CENTER_MIDDLE]);
            line.setAttribute('y2', to_square.top + to_square.height/2 + to_diff_y[CENTER_MIDDLE]);
        }


        //
        //             to_square
        //            from_square

        else if(from_square_below(from_square, to_square)
            &&  !from_square_from_left(from_square, to_square)
            &&  !from_square_from_right(from_square, to_square)
        ) {
            line.setAttribute('x1', from_square.left + from_square.width/2 + from_diff_x[MIDDLE_DOWN]);
            line.setAttribute('y1', from_square.top + from_diff_y[MIDDLE_DOWN]);
            line.setAttribute('x2', to_square.left + to_square.width/2 + to_diff_x[CENTER_MIDDLE]);
            line.setAttribute('y2', to_square.top + to_square.height + to_diff_y[CENTER_MIDDLE]);
        }

        //
        //              to_square
        // from_square

        else if(from_square_below(from_square, to_square)
            &&  from_square_from_left(from_square, to_square)
        ) {
            line.setAttribute('x1', from_square.left + from_square.width/2 + from_diff_x[LEFT_DOWN]);
            line.setAttribute('y1', from_square.top + from_diff_y[LEFT_DOWN]);
            line.setAttribute('x2', to_square.left + to_diff_x[CENTER_MIDDLE]);
            line.setAttribute('y2', to_square.top + to_square.height/2 + to_diff_y[CENTER_MIDDLE]);
        }
    };

    container.append(line);
}
