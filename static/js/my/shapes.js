function Shapes() {
    var shapes = {};
    var shape_types = {};

    this.add_shape = function(shape_id, shape) {
        shapes[shape_id] = shape;
    };

    this.delete_shape = function (shape_id) {
        delete shapes[shape_id];
    };

    this.add_new_square = function(shape_id, square_json) {
        var position = square_json["position"];

        var connected_to = null;
        if("connected_to" in square_json) {
            if("to" in square_json["connected_to"]) {
                connected_to = square_json["connected_to"]["to"]
            }
        }

        var square = new Square(
            new Coord(position["x"], position["y"]),
            shape_id,
            square_json["description"],
            square_json["executor"],
            connected_to
        );
        shapes[shape_id] = square;
        shape_types[shape_id] = "square"
    };

    this.add_new_rhombus = function(shape_id, square_json) {
        var position = square_json["position"];

        var connected_to = null;
        if("connected_to" in square_json) {
            if("to" in square_json["connected_to"]) {
                connected_to = square_json["connected_to"]["to"]
            }
        }

        shapes[shape_id] = new Rhombus(
            new Coord(position["x"], position["y"]),
            shape_id,
            square_json["description"],
            square_json["executor"],
            connected_to
        );
        shape_types[shape_id] = "rhombus"
    };

    this.get_shape_type = function(shape_id) {
        return shape_types[shape_id];
    };



    this.save_info = function(id){
        shapes[id].save_info();
    }
}
