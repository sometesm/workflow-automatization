function request(url, data) {
    var square = null;
    $.ajax({
        type: "POST",
        cache: false,
        async: false,
        url: url,
        data: data,
        success: function(data) {
            square = data;
        },
        error: function(error) {
            console.log(error);
        }
    });

    return $(square);
}
