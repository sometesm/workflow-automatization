function Square(coord, id, description, executor, connected_to) {
    var div_html = request("get_square", {title: id, description: description, executor: executor});

    function set_body(description) {
        div_html.find(".body").text(description);
    }
    function set_role(executor) {
        div_html.find(".role").find("b").text(executor);
    }
    function set_title(title) {
        div_html.find(".title").text(title);
    }

    function setting_up(id) {
        div_html.css({ left:coord.x, top:coord.y });
        div_html.css("position", "absolute");
        div_html.attr("id", id);

        set_title(id);
        set_body(description);
        set_role(executor);
    }

    function append() {
        $("#container").append(div_html);
    }

    function save_position(coord) {
        $.ajax({
            type: "POST",
            cache: false,
            async: true,
            url: "/save_position_of_shape/" + id,
            data: {x:coord.left, y:coord.top},
            success: function(data) {
            },
            error: function(error) {
                console.log("error");
                console.log(error);
            }
        });
    }

    this.save_info = function() {
        var form = $("#form");
        var form_fields = form.serializeArray();

        console.log(form_fields);

        $.ajax({
            type: "POST",
            cache: false,
            async: true,
            url: "/save_info/" + id,
            data: form.serialize(),
            success: function(data) {
                var right_panel = $(".right-panel");
                right_panel.removeClass("w3-animate-right");
                right_panel.addClass("animated");
                right_panel.addClass("bounceOutRight");
                right_panel.animate();
                setTimeout(function(){
                    right_panel.hide();
                    right_panel.removeClass("animated");
                    right_panel.removeClass("bounceOutRight");
                    right_panel.addClass("w3-animate-right");
                }, 1000);


                for(var form_field in form_fields) {
                    var name = form_fields[form_field].name;
                    var value = form_fields[form_field].value;

                    switch(name) {
                        case "title":
                            set_title(value);
                            break;
                        case "description":
                            set_body(value);
                            break;
                        case "executor":
                            set_role(value);
                            break;
                        case "connected_to":
                            lines.delete_line(id + "__to__" + connected_to);
                            connected_to = value;
                            lines.add_line(id, connected_to);
                            break;
                    }
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
        return false;
    };

    function bind() {
        var div = $("#" + id);
        var title = div.find(".draggable");
        var body = div.find(".body");

        title.mousemove(function( event ) {
            lines.redraw_lines_about_shape(id);
        });

        title.mouseout(function( event ) {
            var coord = div.position();
            save_position(coord)
        });

        body.click(function( event ) {
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "/get_form/" + id,
                data: {},
                success: function(data) {
                    var f = $("#right-panel-form");
                    f.html(data);

                    var right_panel = $(".right-panel");
                    right_panel.show();
                },
                error: function(error) {

                }
            });
        });
    }

    setting_up(id);
    append();
    bind();
}
