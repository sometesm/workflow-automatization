function Lines(svg_container) {
    var connectors = {};

    this.add_line = function(from_shape_id, to_shape_id) {
        var line_id = from_shape_id + "__to__" + to_shape_id;
        var line = new Line(line_id, from_shape_id, to_shape_id, svg_container);
        connectors[line_id] = line;
        line.draw();
    };

    this.delete_line = function (line_id) {
        delete connectors[line_id];
        $("#" + line_id).remove();
    };

    this.redraw_lines_about_shape = function(line_id) {
        for(var connector in connectors) {
            connectors[connector].draw();
        }
    };
}
