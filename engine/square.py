import json
from flask import render_template


class Square:
    def __init__(self, id):
        self.id = id

    def save_info(self, request, shapes_json):
        form = request.form
        shape = shapes_json[self.id]

        print(form)

        if "executor" in form:
            shape["executor"] = form["executor"]

        if "description" in form:
            shape["description"] = form["description"]

        if "connected_to" in form:
            if form["connected_to"] != "undefined":
                shape["connected_to"] = {"to": form["connected_to"], "type": "solid"}
            else:
                shape["connected_to"] = {}

        with open("shapes.json", "w") as f:
            json.dump(shapes_json, f, indent=4)

        return '{ "success": true, "message": "Position of ' + self.id + ' saved" }'

    def get_form(self, shape, shapes_json):
        if "executor" in shape:
            executor = shape["executor"]
        else:
            executor = "undefined"

        if "connected_to" in shape:
            connected_to_shape = shape["connected_to"]["to"]
        else:
            connected_to_shape = "undefined"

        all_shapes = [cn for cn in shapes_json]

        return render_template(
            "forms/square.html",
            id=self.id,
            description=shape["description"],
            connected_to=connected_to_shape,
            executors=["Yuriy Gyerts", "Anastasia Boyton"],
            all_shapes=all_shapes,
            executor=executor
        )
